package com.uberdos.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uberdos.main.entities.TipoViaje;
import com.uberdos.main.entities.Viaje;
import com.uberdos.main.entities.Usuario;
import com.uberdos.main.repositories.ITipoViajeRepository;
import com.uberdos.main.repositories.IViajeRepository;
import com.uberdos.main.repositories.IUsuarioRepository;

@Service
@Transactional
public class PaViajeService {
	
		@Autowired
		IViajeRepository rViaje;
		
		@Autowired
		ITipoViajeRepository rTipo;
		
		@Autowired
		IUsuarioRepository rUsu;
		
		
		public List<Viaje> findAll(){
			List<Viaje> lista = (List<Viaje>) rViaje.findAll();
			return lista;	
		}
		
		public void save(Viaje viaje) {
			 rViaje.save(viaje);
		}
		
		public void delete(Integer id) {
			rViaje.deleteById(id);
		}
		
		public List<TipoViaje> findAllTipoViaje(){
			List<TipoViaje>  lista = (List<TipoViaje>) rTipo.findAll();
			return lista;
		}
		
		public TipoViaje findByIdTipoViaje(Integer id) {
			return rTipo.findById(id).get();
		}
		
		public List<Usuario> findAllUsuario(){
			List<Usuario> listaU = (List<Usuario>) rUsu.findAll();
			return listaU;
		}
		
		public Usuario findByIdUsuario(Long id) {
			return rUsu.findById(id).get();
			
		}
}
