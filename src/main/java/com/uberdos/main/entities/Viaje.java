package com.uberdos.main.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pa_viaje")
public class Viaje {
	
	//Atributos de la clase Pa Viaje
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_viaje")
	private Integer id;
	
	@Column(name = "pun_par_viaje")
	private String partida;
	
	@Column(name = "pun_des_viaje")
	private String destino;
	
	@Column(name = "fec_ini_viaje")
	private Date finicio;
	
	@Column(name = "fec_fin_viaje")
	private Date ffin;
	
	@Column(name = "hor_par_viaje")
	private Date hpartida;
	
	@Column(name = "hor_ret_viaje")
	private Date hdestino;
	
	@Column(name = "precio_viaje")
	private double precio;
	
	@Column(name = "est_viaje")
	private boolean estado;
	
	@JoinColumn(name = "tipo_viaje_id")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private TipoViaje id_tipo;
	
	@JoinColumn(name = "usuario_id")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Usuario id_usuario;

	
	//Getter and Setter
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPartida() {
		return partida;
	}

	public void setPartida(String partida) {
		this.partida = partida;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Date getFinicio() {
		return finicio;
	}

	public void setFinicio(Date finicio) {
		this.finicio = finicio;
	}

	public Date getFfin() {
		return ffin;
	}

	public void setFfin(Date ffin) {
		this.ffin = ffin;
	}

	public Date getHpartida() {
		return hpartida;
	}

	public void setHpartida(Date hpartida) {
		this.hpartida = hpartida;
	}

	public Date getHdestino() {
		return hdestino;
	}

	public void setHdestino(Date hdestino) {
		this.hdestino = hdestino;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public TipoViaje getId_tipo() {
		return id_tipo;
	}

	public void setId_tipo(TipoViaje id_tipo) {
		this.id_tipo = id_tipo;
	}

	public Usuario getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Usuario id_usuario) {
		this.id_usuario = id_usuario;
	}

}
