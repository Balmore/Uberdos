package com.uberdos.main.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pa_cuenta_bancaria")
public class CuentaBancaria {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_cuenta;
	private String nom_banco;
	private String prop_cuenta;
	private String num_tar_cuenta;
	private Date fec_ven_cuenta;
	private int cvv_cuenta;
	private String bic_cuenta;
	private boolean est_cuenta;
	@JoinColumn(name = "usuario_id")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Usuario usuario;
	public int getId_cuenta() {
		return id_cuenta;
	}
	public void setId_cuenta(int id_cuenta) {
		this.id_cuenta = id_cuenta;
	}
	public String getNom_banco() {
		return nom_banco;
	}
	public void setNom_banco(String nom_banco) {
		this.nom_banco = nom_banco;
	}
	public String getProp_cuenta() {
		return prop_cuenta;
	}
	public void setProp_cuenta(String prop_cuenta) {
		this.prop_cuenta = prop_cuenta;
	}
	public String getNum_tar_cuenta() {
		return num_tar_cuenta;
	}
	public void setNum_tar_cuenta(String num_tar_cuenta) {
		this.num_tar_cuenta = num_tar_cuenta;
	}
	public Date getFec_ven_cuenta() {
		return fec_ven_cuenta;
	}
	public void setFec_ven_cuenta(Date fec_ven_cuenta) {
		this.fec_ven_cuenta = fec_ven_cuenta;
	}
	public int getCvv_cuenta() {
		return cvv_cuenta;
	}
	public void setCvv_cuenta(int cvv_cuenta) {
		this.cvv_cuenta = cvv_cuenta;
	}
	public String getBic_cuenta() {
		return bic_cuenta;
	}
	public void setBic_cuenta(String bic_cuenta) {
		this.bic_cuenta = bic_cuenta;
	}
	public boolean isEst_cuenta() {
		return est_cuenta;
	}
	public void setEst_cuenta(boolean est_cuenta) {
		this.est_cuenta = est_cuenta;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
