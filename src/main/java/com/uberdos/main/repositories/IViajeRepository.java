package com.uberdos.main.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uberdos.main.entities.Viaje;

@Repository
public interface IViajeRepository extends CrudRepository<Viaje, Integer>{
		
}
