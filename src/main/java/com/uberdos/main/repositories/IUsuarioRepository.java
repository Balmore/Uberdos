package com.uberdos.main.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uberdos.main.entities.Usuario;;
@Repository
public interface IUsuarioRepository extends CrudRepository<Usuario, Long> {
	
	public Usuario findByEmail(String email);
	
	public List<Usuario> findByTipo(String tipo);
}
