package com.uberdos.main.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uberdos.main.entities.CuentaBancaria;

@Repository
public interface ICuentaBancariaRepository extends CrudRepository<CuentaBancaria, Integer> {

}
