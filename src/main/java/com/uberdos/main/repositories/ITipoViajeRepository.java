package com.uberdos.main.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.uberdos.main.entities.TipoViaje;

@Repository
public interface ITipoViajeRepository extends CrudRepository<TipoViaje, Integer>{

}
