package com.uberdos.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uberdos.main.entities.CuentaBancaria;
import com.uberdos.main.repositories.ICuentaBancariaRepository;

@Service
@Transactional
public class CuentaBancariaService {
	
	@Autowired
	ICuentaBancariaRepository cuentaBancariaRepository;
	
	public List<CuentaBancaria> findAll(){
		return (List<CuentaBancaria>) cuentaBancariaRepository.findAll();
	}
	
}
