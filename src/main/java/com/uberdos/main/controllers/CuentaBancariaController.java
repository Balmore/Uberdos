package com.uberdos.main.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uberdos.main.entities.CuentaBancaria;
import com.uberdos.main.entities.Usuario;
import com.uberdos.main.repositories.IUsuarioRepository;
import com.uberdos.main.service.CuentaBancariaService;

@Controller
@RequestMapping(value = "/cuentaBancaria")
public class CuentaBancariaController {
	
	@Autowired
	CuentaBancariaService cuentaBancariaService;

	@Autowired
	IUsuarioRepository usuarioRepository;
	
	@GetMapping("/cuentaBancaria")
	public String cuentaBancariaView(Model model, Principal principal, CuentaBancaria cuentaBancaria) throws Exception {
		if (principal == null) {
			return "index";
		}
		
		Usuario user = usuarioRepository.findByEmail(principal.getName());
		
		model.addAttribute("usuario", user);
		model.addAttribute("cuentaBancaria", cuentaBancaria);
		return "cuentaBancariaIndex";
	}
	
	
	
	@PostMapping("/save")
	public String save(CuentaBancaria cuentaBancaria, HttpServletRequest req, BindingResult result) {
		int id = (req.getParameter("id") != "")? Integer.parseInt(req.getParameter("id")) : 0;
		
		return "";
	}
	
}
