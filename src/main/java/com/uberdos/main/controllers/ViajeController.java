package com.uberdos.main.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uberdos.main.entities.TipoViaje;
import com.uberdos.main.entities.Viaje;
import com.uberdos.main.entities.Usuario;
import com.uberdos.services.PaViajeService;

@Controller
@RequestMapping(value= "/viaje")
public class ViajeController {
	
	@Autowired
	PaViajeService sViaje;
	
	@GetMapping("inicio")
	public String register(Model model, Viaje viaje ) {
		return  "viaje/inicio";
	}
	
	@GetMapping("publicar")
	public String publicar(Model model) {
		List<Viaje> lista = sViaje.findAll();
		model.addAttribute("lista", lista);
		
		return "publicar";
	}
	
	@GetMapping("delete/{id}")
	public String delete(@PathVariable Integer id) {
		sViaje.delete(id);
		return "redirect:/viaje/inicio";	
	}
	
	@GetMapping("publicar")
	public String add(Model model) {
		List<TipoViaje> tipov = sViaje.findAllTipoViaje();
		model.addAttribute("listTipoV", tipov);
		
		List<Usuario> usu = sViaje.findAllUsuario();
		model.addAttribute("listUsu", usu);
		
		return "viaje/publicar";
	}
	
	@PostMapping("guardar")
	public String guardar(HttpServletRequest req) throws ParseException {
		Viaje viaje = new Viaje();
		
		if(!req.getParameter("id").equals("")) {
			viaje.setId(Integer.parseInt(req.getParameter("id")));
		}
		
		viaje.setPartida(req.getParameter("partida"));
		viaje.setDestino(req.getParameter("destino"));
		Date finicio = new SimpleDateFormat("yyyy/MM/dd").parse(req.getParameter("finicio"));
		viaje.setFinicio(finicio);
		Date ffin = new SimpleDateFormat("yyyy/MM/dd").parse(req.getParameter("ffin"));
		viaje.setFfin(ffin);
		Date hpartida = new SimpleDateFormat("hh:mm:ss a").parse(req.getParameter("hpartida"));
		viaje.setHpartida(hpartida);
		Date hdestino = new SimpleDateFormat("hh:mm:ss a").parse(req.getParameter("hdestino"));
		viaje.setHdestino(hdestino);
		String estado = "true";
		boolean est = Boolean.parseBoolean(req.getParameter(estado));
		viaje.setEstado(est);
		
		Integer id_tipo = Integer.parseInt(req.getParameter("id_tipo"));
		TipoViaje tipo = sViaje.findByIdTipoViaje(id_tipo);
		
		viaje.setId_tipo(tipo);
		
		Long id_usu = Long.parseLong(req.getParameter("id_usuario"));
		Usuario usu = sViaje.findByIdUsuario(id_usu);
		
		viaje.setId_usuario(usu);
	
		sViaje.save(viaje);
		
		return "redirect:/inicio/publicar";
		
	}
	
}
